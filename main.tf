# module-kms/main.tf

resource "aws_kms_key" "key" {
  for_each                = var.keys
  description             = each.value.description
  deletion_window_in_days = each.value.deletion_window_in_days

  tags = {
    Environment = var.environment
    Managed_by  = var.managed_by
  }

}
