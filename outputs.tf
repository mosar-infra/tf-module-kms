# module kms outputs

output "kms_key" {
  value     = aws_kms_key.key
  sensitive = true
}
